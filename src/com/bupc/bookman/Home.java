package com.bupc.bookman;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.bupc.bookman.R;

public class Home extends Activity {
	ImageButton btnBrowse, btnMerge, btnInsert, btnSplit;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home);

		btnBrowse = (ImageButton) findViewById(R.id.btnBrowse);
		btnMerge = (ImageButton) findViewById(R.id.btnMerge);
		btnInsert = (ImageButton) findViewById(R.id.btnInsert);

		btnBrowse.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				Intent i = new Intent(Home.this, ChooseFileActivity.class);
				startActivity(i);
			}
		});
		
		btnMerge.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				Intent i = new Intent(Home.this, MergerActivity.class);
				startActivity(i);
			}
		});
		
		btnInsert.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				Intent i = new Intent(Home.this, InsertActivity.class);
				startActivity(i);
			}
		});
		
		

	}

}
