package com.bupc.bookman;

import java.io.File;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bupc.bookman.R;

public class SplitActivity extends Activity {
	private static final int REQUEST_PATH = 1;
	Button btnInsert, btnFile1, btnFile2;
	EditText file1, file2;
	String curFileName;
	private String btnn;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.split_activity);
		
		btnInsert = (Button) findViewById(R.id.btnInsert);
		btnFile1 = (Button) findViewById(R.id.btnFile1);
		btnFile2 = (Button) findViewById(R.id.btnFile2);
		file1 = (EditText) findViewById(R.id.pathFile1);
		file2 = (EditText) findViewById(R.id.pathFile2);
		
		btnInsert.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				String path1 = file1.getText().toString();
				String path2 = file2.getText().toString();
				String exDir = Environment.getExternalStorageDirectory()+"/";
				File aA = new File(path1);
	    		File aA2 = new File(path2);
	    		
	    		String bB =aA.getName();
	    		String bB2 =aA2.getName();
	    		
	    		String cC = bB.replace(".pdf", "");
	    		String cC2 = bB2.replace(".pdf", "");
	    		
				String newFileName = exDir+cC+"+"+cC2+".pdf";
				File newFilePath = new File(newFileName);
				
				
				
				PdfMerge merge = new PdfMerge();
				merge.merge(path1, path2);
                if (merge.merge(path1, path2)) {
                	        	  pdfView(newFilePath);
                } else {
                	Toast.makeText(getApplicationContext(),
                            "Invalid input/file!", Toast.LENGTH_SHORT)
                            .show();
                }
			}
		});
		
		btnFile1.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				btnn = "File1";
				Intent intent1 = new Intent(SplitActivity.this, FileChooser.class);
		        startActivityForResult(intent1,REQUEST_PATH);
			}
		});
		btnFile2.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				btnn = "File2";
				Intent intent1 = new Intent(SplitActivity.this, FileChooser.class);
		        startActivityForResult(intent1,REQUEST_PATH);
			}
		});
			
	}
 // Listen for results.
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        // See which child activity is calling us back.
    	if (requestCode == REQUEST_PATH){
    		if (resultCode == RESULT_OK) { 
    			curFileName = data.getStringExtra("GetFilePath");
    			if(btnn == "File1"){
    				file1.setText(curFileName);
    			} else {
    				file2.setText(curFileName);
    			}
            	
    		}
    	 }
    }
	public void pdfView(File f) {
		
		Intent intent = new Intent();
		intent.setDataAndType(Uri.fromFile(f), "application/pdf");
		intent.setClass(this, OpenFileActivity.class);
		intent.setAction("android.intent.action.VIEW");
		this.startActivity(intent);
	}
}
