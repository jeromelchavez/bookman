package com.bupc.bookman;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import android.net.Uri;
import android.os.Environment;
import android.util.Log;






import android.widget.Toast;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfReader;



public class PdfMerge {
	public Boolean insert(String path1, String path2, int pageNum) {
    	try{
    			
    		String exDir = Environment.getExternalStorageDirectory()+"/";
    		String[] files = { path1, path2 };
    		String pf1 = path1;
    		String pf2 = path2;
    		File aA = new File(path1);
    		String bB =aA.getName();
    		String cC = bB.replace(".pdf", "[");
			String newFileName = exDir+cC.toString()+"edited].pdf";
    		
    		
    		PdfReader ppf1 = new PdfReader(pf1);
    		int pdfPages1 = ppf1.getNumberOfPages();
    		ppf1.close();
    		PdfReader ppf2 = new PdfReader(pf2);
    		int pdfPages2 = ppf2.getNumberOfPages();
    		ppf1.close();
    		int a = pdfPages1 + 1;
    		int b = pageNum - 1;
    		int totalPages = pdfPages1 + pdfPages2;
    		if( pageNum > pdfPages1 || pageNum <= 1 ) {
    			return false;
    		
    		} else {
    		Document document = new Document();
    		        
    		PdfCopy copy = new PdfCopy(document, new FileOutputStream(exDir+"merge.pdf"));
    		        
    		document.open();
    		        
    		PdfReader reader;
    		int n;
    		        
    		for (int i = 0; i < files.length; i++) {
    			reader = new PdfReader(files[i]);
    		            
    			n = reader.getNumberOfPages();
    		    for (int page = 0; page < n; ) {
    		         copy.addPage(copy.getImportedPage(reader, ++page));
    		        	}
    		    copy.freeReader(reader);
    		    reader.close();
    		   }
    		        
    		document.close();
    		
    		
    		String bullShit = exDir+"merge.pdf";
    		
    		File temp = new File(bullShit);
    		
    		Document document2 = new Document();
    		PdfCopy copy2 = new PdfCopy(document2, new FileOutputStream(newFileName));
    		document2.open();
    		PdfReader reader2 = new PdfReader(bullShit);
    		int num = reader2.getNumberOfPages();

    			reader2.selectPages("1-"+b+", "+a+"-"+totalPages+", "+pageNum+"-"+pdfPages1);
	            
    			for (int page2 = 0; page2 < num; ) {
    				copy2.addPage(copy2.getImportedPage(reader2, ++page2));
	        		}
    			copy2.freeReader(reader2);
    			reader2.close();
    		
	        // step 5
    		document2.close();
    		temp.delete();
    		return true;
    		}
    	} catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (DocumentException e) {
            e.printStackTrace();
            return false;
        }
    	
    }
	public Boolean merge(String path1, String path2) {
    	try{
    			
    		String exDir = Environment.getExternalStorageDirectory()+"/";
    		String[] files = { path1, path2 };
    		
    		File aA = new File(path1);
    		File aA2 = new File(path2);
    		
    		String bB =aA.getName();
    		String bB2 =aA2.getName();
    		
    		String cC = bB.replace(".pdf", "");
    		String cC2 = bB2.replace(".pdf", "");
    		
			String newFileName = exDir+cC+"+"+cC2+".pdf";
    		Document document = new Document();
    		        
    		PdfCopy copy = new PdfCopy(document, new FileOutputStream(newFileName));
    		        
    		document.open();
    		        
    		PdfReader reader;
    		int n;
    		        
    		for (int i = 0; i < files.length; i++) {
    			reader = new PdfReader(files[i]);
    			n = reader.getNumberOfPages();
    		    for (int page = 0; page < n; ) {
    		         copy.addPage(copy.getImportedPage(reader, ++page));
    		        	}
    		    copy.freeReader(reader);
    		    reader.close();
    		   }  
    		document.close();
    		return true;
    	} catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (DocumentException e) {
            e.printStackTrace();
            return false;
        }
    	
    }
	public Boolean split(String path, int wPage) {
    	try{
    			
    		String exDir = Environment.getExternalStorageDirectory()+"/";
    		PdfReader file = new PdfReader(path);
    		int totalPage = file.getNumberOfPages();
    		file.close();
    		int a = wPage + 1;
    		File aFile = new File(path);
    		String bFile = aFile.getName();
    		String cFile = bFile.replace(".pdf", "");
    		String file1 = exDir+cFile+"1.pdf";
    		String file2 = exDir+cFile+"2.pdf";
    		
    	//file1
    		Document document = new Document();
    		PdfCopy copy = new PdfCopy(document, new FileOutputStream(file1));
    		document.open();
    		PdfReader reader = new PdfReader(path);
    			int n = reader.getNumberOfPages();
    			reader.selectPages("1-"+wPage);
    		    for (int page = 0; page < n; ) {
    		         copy.addPage(copy.getImportedPage(reader, ++page));
    		        	}
    		    copy.freeReader(reader);
    		    reader.close();
    		document.close();
    	//file2
    		Document document2 = new Document();
    		PdfCopy copy2 = new PdfCopy(document2, new FileOutputStream(file2));
    		document2.open();
    		PdfReader reader2 = new PdfReader(path);
    			int n2 = reader.getNumberOfPages();
    			reader2.selectPages(a+"-"+totalPage);
    		    for (int page2 = 0; page2 < n2; ) {
    		         copy2.addPage(copy2.getImportedPage(reader2, ++page2));
    		        	}
    		    copy2.freeReader(reader2);
    		    reader2.close();
    		document2.close();
    		return true;
    	} catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (DocumentException e) {
            e.printStackTrace();
            return false;
        }
    	
    }
	
}