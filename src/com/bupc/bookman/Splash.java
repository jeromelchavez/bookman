package com.bupc.bookman;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.widget.ImageView;
import android.widget.Toast;
import com.bupc.bookman.R;

public class Splash extends Activity {
	ImageView iv1;
	AnimationDrawable Anim;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		/*
		Toast.makeText(getApplicationContext(), "BookMan", Toast.LENGTH_SHORT)
				.show();

		Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		vibrator.vibrate(50);
		*/

		iv1 = (ImageView) findViewById(R.id.SplashImg);
		try {
			Anim = new AnimationDrawable();

			iv1.setBackgroundResource(R.drawable.splashanim);
			final Handler handler = new Handler();
			handler.postDelayed(new Runnable() {

				public void run() {

					Anim.start();

					Intent i = new Intent(Splash.this, Home.class);
					startActivity(i);

					finish();

				}
			}, 2000);

		} catch (Exception e) {
		}
	}

}